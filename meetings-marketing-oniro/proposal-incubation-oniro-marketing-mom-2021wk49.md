# Oniro WG Incubation stage marketing wk49

Chair: Agustin
Scriba: Agustin
Schedule: 2021-12-08

## Participants

(add/erase participants)

Shanda (EF), Yves (EF), Agustin B.B.(EF), Gael (EF), Adrian (Huawei), Patrick O. (NOI), Dony (Huawei), Aurore P. (Huawei), Andrea G. (Linaro)

## Agenda

* Events List - Chiara 15 min
* AOB - 15 min

## MoM

### Events List

Presentation

* Template plus initial event list created by Agustin: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/Events-oniro
* Ticket where we keep track of this action: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/issues/4

Discussion

* Linaro had no booth at Embedded World and MWC 2021
* Gael will provide the OSXP dates. They are already published.
* Please send your input. We need all the events to discuss them next week.
* Since different people use different tools we need to come up with the maximum common denominator. That could be the wiki pages at gitlab instead of a spreadsheet or a file in a git repo. We'll see.

### AOB

FOSDEM

* There are several rooms to follow. Most CfPs ends around Dec 20th
   * Distributions #link https://fosdem.org/2022/schedule/track/distributions/
   * Embedded, mobile and automotive #link https://fosdem.org/2022/schedule/track/embedded_mobile_and_automotive/
   * Legal and Policy Issues DevRoom #link https://fosdem.org/2022/schedule/track/legal_and_policy_issues_devroom/
   * Micro kernel and component based OS #link https://fosdem.org/2022/schedule/track/microkernel_and_component_based_os/ 
   * Testing and Automation #link https://fosdem.org/2022/schedule/track/testing_and_automation/ 
* Philippe Coval will coordinate this.
* We had 7 talks on 2021
* Huawei had a virtual booth last year. We want an Oniro booth.
   * #task Agustin will request a booth for this year.

Topics for next week
* Events list: agustin (unless Chiara takes the lead)
* Demonstrator vs blueprint: agustin
* Demonstrators: definition and roadmap. Adrian.

Patrick would like to create a "demonstrator corner" at NOI. For that, he 
needs to understand the roadmap.

## Relevant links

* Repo: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/
* Actions workflow board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings 
